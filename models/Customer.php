<?php

namespace app\models;

use splynx\v2\models\customer\BaseCustomer;

/**
 * Class Customer
 * @package app\models
 */
class Customer extends BaseCustomer
{
    /**
     * @return Customer[]|null
     */
    public function getFiveLastAdded(): ?array
    {
        return $this->findAll(
            [],
            [],
            [
                'id' => 'DESC',
            ],
            5
        );
    }
}
