<?php

namespace app\commands;

use splynx\base\BaseInstallController;

/**
 * Class InstallController
 * @package app\commands
 */
class InstallController extends BaseInstallController
{
    /**
     * @inheritdoc
     */
    public $module_status = self::MODULE_STATUS_ENABLED;

    /**
     * @inheritdoc
     */
    public static $minimumSplynxVersion = '3.1';

    /**
     * @inheritdoc
     */
    public function getAddOnTitle(): string
    {
        // Db column varchar(64)
        return 'Splynx Add-on Skeleton';
    }

    /**
     * @inheritdoc
     */
    public function getModuleName(): string
    {
        // Db column varchar(32)
        return 'splynx_addon_skeleton';
    }

    /**
     * @inheritdoc
     */
    public function getApiPermissions(): array
    {
        return [
            [
                'controller' => 'api\admin\administration\Administrators',
                'actions' => ['index', 'view'],
            ],
            [
                'controller' => 'api\admin\customers\Customer',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getEntryPoints(): array
    {
        return [
            [
                'name' => 'skeleton_main',
                'title' => $this->getAddOnTitle(),
                'root' => 'controllers\admin\CustomersController',
                'place' => 'admin',
                'url' => '%2Fskeleton%2F',
                'icon' => 'fa-puzzle-piece',
            ],
        ];
    }
}
