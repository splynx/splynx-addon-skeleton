<?php

namespace app\assets;

use splynx\assets\HelperAsset;
use yii\bootstrap\BootstrapAsset;
use yii\web\AssetBundle;
use yii\web\View;
use yii\web\YiiAsset;

/**
 * Class AppAsset
 * @package app\assets
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    /**
     * @var string[]
     */
    public $css = [
        'css/site.css',
    ];

    /**
     * @var string[]
     */
    public $js = [
    ];

    /**
     * @var string[]
     */
    public $depends = [
        YiiAsset::class,
        BootstrapAsset::class,
        HelperAsset::class,
    ];

    /**
     * @var array<string, int>
     */
    public $jsOptions = ['position' => View::POS_HEAD];
}
