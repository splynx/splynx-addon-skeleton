<?php

namespace app\controllers;

use app\models\Customer;
use yii\base\UnknownClassException;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ErrorAction;
use yii\web\View;

/**
 * Class SiteController
 * @package app\controllers
 */
class SiteController extends Controller
{
    /**
     * @return array<string, array<string, string>>
     */
    public function actions(): array
    {
        return [
            'error' => [
                'class' => ErrorAction::class,
            ],
        ];
    }

    /**
     * @return array<string, string|array<string,mixed>>
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     * @throws UnknownClassException
     */
    public function actionIndex(): string
    {
        /** @var View $view */
        $view = $this->getView();
        $view->title = 'Splynx Add-on Skeleton';

        return $this->render('index', [
            'model' => new Customer(),
        ]);
    }
}
