<?php

namespace app\components;

use splynx\components\BaseValidateConfig;

/**
 * Class ValidateConfig
 * @package app\components
 */
class ValidateConfig extends BaseValidateConfig
{
}
